# Installing Apex Legends via Lutris

**WARNING: USING THIS CAN GET YOU BANNED PERMANENTLY, USE IT AT YOUR OWN RISK.**

I wrote this guide because I had troubles installing the game, so I decided to write this to help people with similar issues.

You can get the wine build for installing this game from the [VKx Discord](https://discord.gg/yN3UwM). Look at the pinned messages in the wine-eac-users Channel. If you're experiencing issues, look in the [Troubleshooting section of this guide.](#troubleshooting) If you can't find your issue in there, please report it so i can extend this guide and help fix the issue.

## Installation

 1. Use the [Apex Legends Installer](https://lutris.net/games/apex-legends/) (Not Origin) in Lutris.
 2. When the script is finished: Launch Origin, Login and fully install Apex Legends. **DO NOT LAUNCH IT YET**
 3. Kill all wine processes. You can use: `pkill -9 -f .exe && pkill -9 -f wine` (This will kill all wine processes. If you have unsaved data in wine you probably want to save it first)
 4. After all Wine processes are killed, you can select Apex Legends, go into the Runner Options and switch the Wine version to *lutris-eac-testing-5.5-3-x86_64* build (which I have tested) or another build.
 5. Set Runner Options -> Output Debugging to *Inherit from Environment*.
 6. Install winhttp via winetricks. You can either use Lutris by clicking Wintricks -> Select the default wineprefix -> Install a Windows DLL or component -> winhttp and click OK or use: `WINEPREFIX=YOUR_WINE_PREFIX winetricks winhttp` and replace YOUR_WINE_PREFIX with the **full path** of the game installation folder (If you have another build you may wanna use the winecfg of that build)
 7. Start Origin again.
 8. Go into your Game Library, right-click on Apex Legends, click on Game Properties -> Advanced Launch Options and add `-dev -window -noborder -novid -forcenovsync`
 9. Start the Game
10. You will have very low FPS

## Troubleshooting

1. Installing Origin
   * If Origin fails updating kill all wine processes and start it again via Lutris
   * If this step fails go to the [Site of the script and report the bug there](https://lutris.net/games/apex-legends/)
2. Installing Apex Legends
   * If the install pauses when it's playable, don't kill wine processes and wait a few minutes. If that doesn't work and the play icon is greyed out you may wanna start over.
   * If you get [this](https://camo.githubusercontent.com/3ed05da17f99315561b3d2ff43a536b5bd0b9a10/68747470733a2f2f692e696d6775722e636f6d2f326a36793075332e706e67) DirectX Error, you may want to follow the steps on [the lutris docs](https://github.com/lutris/docs/blob/master/Origin.md), or start over.
   * If your download has been paused due to permission errors, try [disabling ipv6 in the kernel](https://www.pcsuggest.com/disable-ipv6-linux/#Disable_with_kernel_parameter)
   * If you get a blank window on i3wm add `for_window [instance="origin.exe"] floating enable` to your i3 config
3. Killing Wine-processes
   * If this doesn't work kill the processes manually via htop or restart your PC.
4. Switching Wine version
5. Setting Runner Options
6. Installing winhttp
7. Starting Origin
   * If Origin doesn't start, kill all wine Processes and start again
8. Settings the Launch Options
9. Starting Apex Legends
   * If you get Error 1053 try executing `WINEPREFIX=${HOME}/Games/apex-legends ${HOME}/.local/share/lutris/runners/wine/lutris-eac-testing-5.5-3-x86_64/bin/wineboot -u` in your terminal and try again.
   * If the EAC screen disappears with the game screen not appearing, Kill all wine processes and try again
   * If you're getting Error 35: CreateFile failed while starting Apex, kill all wine Processes, make sure no wine processes are running anymore and try again
   * If you keep getting the same EAC Errors you can try to execute the `EasyAntiCheat_Setup.exe` located in YOUR_WINE_PREFIX/drive_c/Program Files (x86)/Origin Games/Apex/EasyAntiCheat (an easy way to do so would be using Run EXE inside wine prefix in Lutris) and repairing Easy Anti Cheat.
   If that still doesn't work you can try wiping the wine prefix and reinstalling(If you find a better/quicker fix let me know).
   * If you have audio crackling you can change `default_fragments=17` and `default-fragments-size-msec=17` in your /etc/pulse/daemon.conf. (Note that this will probably increase your pulseaudio latency and you will probably want to reduce those values as far as they can go to reduce latency)
   * If you still have crackling sound change the line `load-module module-udev-detect` to `load-module module-udev-detect tsched=0` in your /etc/pulse/default.pa